package ru.codeinside.generics;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ValuesSeparator {

    private List<?> listArray;

    public void addVariables(List<Object> list) {
        this.listArray = list;
    }

    public List<?> getSeparatedBy(Class<?> classes) {
        return listArray.stream().filter(classes::isInstance).collect(Collectors.toList());
    }


}
